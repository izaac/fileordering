/**
 * Created by Jorge on 1/5/14.
 */

import groovy.io.FileType


media_files = ["avi", "mp4", "mkv", ]
archives = ["zip", "iso", "tar", "tgz", "gz", "tar.gz", "7z", "rar"]
def withEachTestFile(os, types, Closure closure) {

    path = os.toLowerCase().contains('windows') ? "E:\\" : System.properties['user.home']

    new File(path).eachFileRecurse(FileType.FILES) {

        for (i in types) {
            if (it.name =~ /\.${i}$/) {
                closure.call(it)
            }
        }


    }

}
os = System.properties['os.name']
withEachTestFile(os, media_files, { println it})
withEachTestFile(os, archives, { println it})


